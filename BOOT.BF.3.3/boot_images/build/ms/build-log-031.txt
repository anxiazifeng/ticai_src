Start Time = Wed Jun 21 02:11:51 PDT 2017
#-------------------------------------------------------------------------------
# ENVIRONMENT BEGIN
#-------------------------------------------------------------------------------
declare -x ADS120HOME="/pkg/asw/compilers/arm/ADS1.2"
declare -x ARM12HOME="/pkg/asw/compilers/arm/ADS1.2"
declare -x ARMBIN="/pkg/qct/software/arm/RVDS/5.01bld94/bin64"
declare -x ARMCC41_ASMOPT="--sitelicense"
declare -x ARMCC41_CCOPT="--sitelicense"
declare -x ARMCC41_LINKOPT="--sitelicense"
declare -x ARMCC5_ASMOPT="--sitelicense"
declare -x ARMCC5_CCOPT="--sitelicense"
declare -x ARMCC5_LINKOPT="--sitelicense"
declare -x ARMCONF="/pkg/asw/compilers/arm/ADS1.2/bin"
declare -x ARMDLL="/pkg/asw/compilers/arm/ADS1.2/bin"
declare -x ARMHOME="/pkg/qct/software/arm/RVDS/5.01bld94"
declare -x ARMINC="/pkg/qct/software/arm/RVDS/5.01bld94/include"
declare -x ARMINCLUDE="/pkg/qct/software/arm/RVDS/5.01bld94/include"
declare -x ARMLIB="/pkg/qct/software/arm/RVDS/5.01bld94/lib"
declare -x ARMLMD_LICENSE_FILE="7117@license-wan-arm1"
declare -x ARMROOT="/pkg/qct/software/arm/RVDS/5.01bld94"
declare -x ARMTOOLS="ARMCT5.01"
declare -x ARM_COMPILER_PATH="/pkg/qct/software/arm/RVDS/5.01bld94/bin64"
declare -x BASH_ENV="/usr2/qctecmdr/.bashrc"
declare -x BATCHMODE="ECOMMANDER"
declare -x BLD_ENV_BUILD_ID="M8917LAAAANAZB"
declare -x BLD_ENV_VER_NUM_COMPRESSED="00221"
declare -x BLD_ENV_VER_NUM_DOTTED="00221"
declare -x BMONGR="BUILD"
declare -x BUILDSPEC="KLOCWORK"
declare -x BUILD_CMD="BUILD_ID=JAAIANAZ BUILD_VER=40000000 MSM_ID=8953 HAL_PLATFORM=8953 TARGET_FAMILY=8953 BUILD_ASIC=8953A CHIPSET=msm8953   boot jsdcc CFLAGS=--diag_error=warning USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged"
declare -x BUILD_FIXSIG=" "
declare -x BUILD_ROOT="../.."
declare -x COMMANDER_ARTIFACT_CACHE="/opt/electriccloud/electriccommander/artifact-cache"
declare -x COMMANDER_DATA="/opt/electriccloud/electriccommander"
declare -x COMMANDER_HOME="/opt/electriccloud/electriccommander"
declare -x COMMANDER_HTTPS_PORT="8443"
declare -x COMMANDER_HTTP_PROXY="http://localhost:6800/"
declare -x COMMANDER_JOBID="e6cd37ce-565f-11e7-a218-f0921c14c82c"
declare -x COMMANDER_JOBSTEPID="e6da56ce-565f-11e7-8af8-f0921c133f10"
declare -x COMMANDER_PLUGINS="/opt/electriccloud/electriccommander/plugins"
declare -x COMMANDER_PORT="8000"
declare -x COMMANDER_RESOURCENAME="crm-ubuntu117"
declare -x COMMANDER_SERVER="qso-qswatcmdr.qualcomm.com"
declare -x COMMANDER_SESSIONID="WW48EHBSSBZUGZMO"
declare -x COMMANDER_WORKSPACE="/prj/qct/asw/ecmdr_ws_crm/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914"
declare -x COMMANDER_WORKSPACE_NAME="crm_ws_sd"
declare -x COMMANDER_WORKSPACE_UNIX="/prj/qct/asw/ecmdr_ws_crm/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914"
declare -x COMMANDER_WORKSPACE_WINDRIVE="M:\\BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914"
declare -x COMMANDER_WORKSPACE_WINUNC="\\\\qctdfsrt\\prj\\qct\\asw\\ecmdr_ws_crm\\BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914"
declare -x CORE_BSP_ROOT="../../core/bsp"
declare -x CRMSERVERNAME="crm-ubuntu117"
declare -x CRM_BUILDID="BOOT.BF.3.3-00221-M8917LAAAANAZB-1"
declare -x CRM_DB_HOST="Oil"
declare -x CRM_DB_NAME="AswProductionDataBase"
declare -x DISPLAY="localhost:10.0"
declare -x EC_LOG_ROOT="logs/agent/jagent.log"
declare -x EMAKE_OPTS="--emake-class=CRM_PROD"
declare -x HAVANA_TOOLCHAIN="/pkg/qct/software/arm/Sourcery_G++_Lite/bin"
declare -x HOME="/usr2/aswbldsv"
declare -x HOMEBREW_CACHE="/usr2/qctecmdr/.linuxbrew/cache"
declare -x INFOPATH="/usr2/qctecmdr/.linuxbrew/share/info:/usr2/qctecmdr/.linuxbrew/share/info:/usr2/qctecmdr/.linuxbrew/share/info:/usr2/qctecmdr/.linuxbrew/share/info:/usr2/qctecmdr/.linuxbrew/share/info:/usr2/qctecmdr/.linuxbrew/share/info:"
declare -x JOBNAME="BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914"
declare -x LANG="en_US"
declare -x LANGUAGE="en_US:"
declare -x LC_ALL="en_US.UTF-8"
declare -x LD_LIBRARY_PATH=":/pkg/qct/qctss/linux/ubuntu/lib"
declare -x LINUXBUILDSERVER=""
declare -x LOGNAME="aswbldsv"
declare -x MAIL="/var/mail/aswbldsv"
declare -x MAKE_PATH="/pkg/gnu/make/3.81/bin"
declare -x MANPATH="/usr2/qctecmdr/.linuxbrew/share/man:/usr2/qctecmdr/.linuxbrew/share/man:/usr2/qctecmdr/.linuxbrew/share/man:/usr2/qctecmdr/.linuxbrew/share/man:/usr2/qctecmdr/.linuxbrew/share/man:/usr2/qctecmdr/.linuxbrew/share/man:"
declare -x ODBCSYSINI="/pkg/qct/qctss/linux/ubuntu/12.04/etc"
declare -x OLDPWD
declare -x ONLINUXSERVER="TRUE"
declare -x P4CLIENT="CRM_BOOT.BF.3.3-00221-M8917LAAAANAZB-1"
declare -x P4PORT="aswp401:1666"
declare -x PARTITION_TOOLS_ROOT="../../core/storage/tools/nandbootmbn"
declare -x PATH="/usr2/qctecmdr/.linuxbrew/bin:/pkg/gnu/make/3.81/bin:/pkg/qct/software/python/2.7.5/bin:/pkg/qct/software/arm/RVDS/5.01bld94/bin64:/pkg/gnu/make/3.81/bin:/pkg/qct/software/python/2.7.5/bin:/pkg/qct/software/arm/RVDS/5.01bld94/bin64:/usr2/qctecmdr/.linuxbrew/bin:/usr2/qctecmdr/.linuxbrew/bin:/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914:/usr2/aswbldsv/bin:/pkg/qct/software/ubuntu/python/2.7.5/bin:/pkg/asw/tools/scons/bin:/pkg/asw/compilers/gnu/crosstool/arm/bin:/pkg/asw/tools/bin:/pkg/asw/compilers/arm/ADS1.2/bin:/sbin:/pkg/qct/software/gnu/make/3.81/bin:/pkg/qct/qctss/linux/bin:/usr/local/bin:/usr2/qctecmdr/.linuxbrew/bin:/usr2/qctecmdr/.linuxbrew/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/opt/electriccloud/electriccommander/bin:/pkg/qct/software/electriccloud/electriccommander/prod/bin:/pkg/qct/qctss/linux/bin:/opt/electriccloud/electriccommander/bin:/pkg/qct/software/electriccloud/electriccommander/prod/bin:/pkg/qct/qctss/linux/bin:/usr2/aswbldsv/bin:/pkg/qct/qctss/linux/bin:/opt/electriccloud/electriccommander/bin:/opt/electriccloud/electriccommander/bin:/pkg/qct/software/electriccloud/electriccommander/prod/bin:/pkg/qct/qctss/linux/bin:.:/opt/electriccloud/electriccommander/bin:/pkg/qct/software/electriccloud/electriccommander/prod/bin:/pkg/qct/qctss/linux/bin:/opt/electriccloud/electriccommander/bin:/pkg/qct/software/electriccloud/electriccommander/prod/bin:/pkg/qct/qctss/linux/bin"
declare -x PWD="/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/ms"
declare -x PYTHONBIN="/pkg/qct/software/ubuntu/python/2.7.5/bin"
declare -x PYTHON_PATH="/pkg/qct/software/python/2.7.5/bin"
declare -x QSWATMETRIC_ID="174586982"
declare -x Q_XTENSA_TOOLS="/pkg/qct/software/xtensa"
declare -x RVCT22_ASMOPT="--fastlicense --sitelicense"
declare -x RVCT22_CCOPT="--fastlicense --sitelicense"
declare -x RVCT22_LINKOPT="--fastlicense --sitelicense"
declare -x RVCT30_ASMOPT="--fastlicense --sitelicense"
declare -x RVCT30_CCOPT="--fastlicense --sitelicense"
declare -x RVCT30_LINKOPT="--fastlicense --sitelicense"
declare -x RVCT31_ASMOPT="--sitelicense"
declare -x RVCT31_CCOPT="--sitelicense"
declare -x RVCT31_LINKOPT="--sitelicense"
declare -x RVCT40_ASMOPT="--sitelicense"
declare -x RVCT40_CCOPT="--sitelicense"
declare -x RVCT40_LINKOPT="--sitelicense"
declare -x SCONS_OEM_BUILD_VER="CRM"
declare -x SHELL="/bin/bash"
declare -x SHLVL="8"
declare -x TERM="xterm"
declare -x TOOLS_SCONS_ROOT="../../tools/build/scons"
declare -x USER="aswbldsv"
declare -x USERNAME="c_samrab"
declare -x WRAPPER_ARCH="x86"
declare -x WRAPPER_BIN_DIR="/opt/electriccloud/electriccommander/agent/bin"
declare -x WRAPPER_BITS="64"
declare -x WRAPPER_CONF_DIR="/opt/electriccloud/electriccommander/conf/agent"
declare -x WRAPPER_FILE_SEPARATOR="/"
declare -x WRAPPER_HOSTNAME="crm-ubuntu117"
declare -x WRAPPER_HOST_NAME="crm-ubuntu117"
declare -x WRAPPER_INIT_DIR="/usr2/qctecmdr"
declare -x WRAPPER_LANG="en"
declare -x WRAPPER_OS="linux"
declare -x WRAPPER_PATH_SEPARATOR=":"
declare -x WRAPPER_PID="32467"
declare -x WRAPPER_WORKING_DIR="/opt/electriccloud/electriccommander"
declare -x XDG_SESSION_COOKIE="76fec73b8354d75e30a8f246000000a4-1498035677.741176-1443576642"
declare -x XFILESEARCHPATH="/usr/dt/app-defaults/%L/Dt"
declare -x XTENSAD_LICENSE_FILE="2100@license-tensilica-wan"
declare -x XTENSA_PREFER_LICENSE="GENERIC"
declare -x relwork="\\\\crm-ubuntu117\\CRMBuilds\\BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914"
#-------------------------------------------------------------------------------
# ENVIRONMENT END
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# BUILD BEGIN
#-------------------------------------------------------------------------------
../../tools/build/scons/build/../SCons/scons -f target.scons --tcfgf=8953.target.builds BUILD_ID=JAAIANAZ BUILD_VER=40000000 MSM_ID=8953 HAL_PLATFORM=8953 TARGET_FAMILY=8953 BUILD_ASIC=8953A CHIPSET=msm8953 boot jsdcc CFLAGS=--diag_error=warning USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
scons: Reading SConscript files ...
------------------------------------------------------------------------------
   Loading OEM build system
------------------------------------------------------------------------------
PYTHON Ver    = 2.7.5 (default, Jun 18 2013, 09:39:18) 
[GCC 4.3.4 [gcc-4_3-branch revision 152973]] 
PYTHON cmd    = /afs/localcell/cm/gv2.6/sysname/pkg.@sys/qct/software/ubuntu/python/2.7.5/bin/python2.7 
SCONS Ver     = 2.0.0.final.0 
SCONS QC Ver  = 1.1-00087 (2015/07/02 04:11:47) 
TARGET_ROOT   = /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b 
BUILD_ROOT    = /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images 
BUILD_MS_ROOT = /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/ms 
MBN_ROOT      = /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/ms/bin/JAAIANAZ
BUILD_ID      = JAAIANAZ
BUILD_VER     = 40000000
BUILD_ASIC    = 8953A
MSM_ID        = 8953
CHIPSET       = msm8953
TARGET_FAMILY = 8953
HAL_PLATFORM  = 8953
 
COMMAND_LINE_TARGETS are ['boot', 'jsdcc']
BUILD_TARGETS are        ['boot', 'jsdcc']

** INFO: Found 28 CPUs, running with -j 8


==============================================================================
Reading SConscript files...




------------------------------------------------------------------------------
   Loading Bootloaders build system
------------------------------------------------------------------------------
SBL1 a53              BUILD_ID=JAAIANAZA, TOOL_ENV=linux
** INFO: Begin Target Config: JAAIANAZA
** INFO: Target configuration file: 8953.target.builds
** INFO: Target Config files have changed.  Reparsing
** INFO: Cleaning all target config products for jaaianaza
** INFO: Generating Target Config Files for JAAIANAZA
tcfgparser: Visiting msm identifier: J from: 8953.target.builds
tcfgparser: Visiting platform identifier: A from: 8953.target.builds
tcfgparser: Visiting SoftComponent: boot
tcfgparser: Visiting SoftComponent: mbntoolsconfigure
tcfgparser: Visiting SoftComponent: ram_dump_memory_regions
tcfgparser: Visiting ram config: A from: 8953.target.builds
tcfgparser: Visiting SoftComponent: boot_mem_map
tcfgparser: Visiting flash config: I from: 8953.target.builds
tcfgparser: Visiting rf config: A from: 8953.target.builds
tcfgparser: Visiting air interface: N from: 8953.target.builds
tcfgparser: Visiting application features: AZ from: 8953.target.builds
tcfgparser: Visiting SoftComponent: ext_driver_enabler
tcfgparser: Visiting image type: A from: 8953.target.builds
** INFO: End Target Config: JAAIANAZA
** INFO: ARMTOOLS        = ARMCT5.01
** INFO: ARMTOOLSVERSION = 5
** INFO: ARMBIN          = /pkg/qct/software/arm/RVDS/5.01bld94/bin64
** INFO: ARMLIB          = /pkg/qct/software/arm/RVDS/5.01bld94/lib
** INFO: ARMINC          = /pkg/qct/software/arm/RVDS/5.01bld94/include
** INFO: Loading CORE scripts...
Uart msm_xml = /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/core/buses/uart/config/uart_sbl_8953.xml
configuration files exist
!! WARNING: Invalid environment context for PublishPublicApi 
   called from [/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/core/securemsm/seccfg/build/SConscript]


		****************** vsense_entered *****************


****************** vsense Pick 8953 *****************
****************** vsense compilation done *****************
!! WARNING: Invalid environment context for PublishPublicApi 
   called from [/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/core/wiredconnectivity/qusb/build/qusb_8953.sconscript]
** INFO: Loaded CORE scripts, elapsed time 3.16 sec.
** INFO: Loading BUILD scripts...
** INFO: Loaded BUILD scripts, elapsed time 0.02 sec.
** INFO: sectools_builder: sectools_builder: msm_jtag_mapping_file= /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/msm_jtag_mapping.xml
** INFO: Begin Target Config: JAAIANAZA
** INFO: Target configuration file: 8953.target.builds
** INFO: End Target Config: JAAIANAZA


------------------------------------------------------------------------------
   Loading Flash Tools build system
------------------------------------------------------------------------------
JSDCC a53             BUILD_ID=JAAIANAZA, TOOL_ENV=linux
** INFO: Begin Target Config: JAAIANAZA
** INFO: Target configuration file: 8953.target.builds
** INFO: End Target Config: JAAIANAZA
** INFO: ARMTOOLS        = ARMCT5.01
** INFO: ARMTOOLSVERSION = 5
** INFO: ARMBIN          = /pkg/qct/software/arm/RVDS/5.01bld94/bin64
** INFO: ARMLIB          = /pkg/qct/software/arm/RVDS/5.01bld94/lib
** INFO: ARMINC          = /pkg/qct/software/arm/RVDS/5.01bld94/include
** INFO: Loading CORE scripts...
configuration files exist
!! WARNING: Invalid environment context for PublishPublicApi 
   called from [/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/core/securemsm/seccfg/build/SConscript]


		****************** vsense_entered *****************


****************** vsense Pick 8953 *****************
****************** vsense compilation done *****************
!! WARNING: Invalid environment context for PublishPublicApi 
   called from [/local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/core/wiredconnectivity/qusb/build/qusb_8953.sconscript]
** INFO: Loaded CORE scripts, elapsed time 2.81 sec.


------------------------------------------------------------------------------
COMMAND_LINE_TARGETS are ['boot', 'jsdcc']
BUILD_TARGETS are        ['boot', 'jsdcc']


==============================================================================
scons: done reading SConscript files.
scons: Cleaning targets ...
scons: done cleaning targets.
** INFO: Cleaning all target config products for jaaianaza
** INFO: Removed /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/ms/custjaaianaza.h
** INFO: Removed /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/ms/targjaaianaza.h
** INFO: Removed /local/mnt/workspace/CRMBuilds/BOOT.BF.3.3-00221-M8917LAAAANAZB-1_20170621_015914/b/boot_images/build/ms/tcfg_jaaianaza.py
** INFO: Cleaning all target config products for jaaianaza
** INFO: Cleaning all target config products for jaaianaza

==============================================================================
   SCons build summary
==============================================================================
** Build time...
 Build start  : Wed Jun 21 02:11:52 2017
 Build end    : Wed Jun 21 02:12:03 2017
 Elapsed time : 0:00:11
#-------------------------------------------------------------------------------
# BUILD END
#-------------------------------------------------------------------------------
Start Time = Wed Jun 21 02:11:51 PDT 2017 - End Time = Wed Jun 21 02:12:03 PDT 2017
Elapsed Time = 12 seconds
