#!/bin/bash
#===============================================================================
#
# CBSP Buils system
#
# General Description
#    build shell script file.
#
# Copyright (c)2015,2017 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
# $Header: //components/rel/boot.bf/3.3/boot_images/build/ms/build.sh#24 $
# $DateTime: 2017/02/02 05:48:34 $
# $Author: pwbldsvc $
# $Change: 12357514 $
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to the module.
# Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     -----------------------------------------------------------
# 01/24/17   kvs     Added support for a new build ID JAAIANAA for IoT
# 04/22/16   qbz     Add DDI compiling support for all platforms
# 01/07/16   sk      Enable JSDCC tools for for Jacala
# 12/30/15   sk      Enable firehose for Jacala
# 11/27/15   yps     Added support for compiling 8952 DDI
# 11/18/15   yps     Add MSM8953 DDI compiling commands
# 10/29/15   sy      Initial version
#===============================================================================
#===============================================================================
# Set up default paths
#===============================================================================
export BUILD_ROOT=../..
export CORE_BSP_ROOT=$BUILD_ROOT/core/bsp
export TOOLS_SCONS_ROOT=$BUILD_ROOT/tools/build/scons

cd `dirname $0`
#-------------Usage --------------
function usage()
{
    echo "==============================================================="
    echo "# This script compiles boot binaries for ONE of the Bear family"
    echo "# i.e.  8952. "
    echo "# Target must be specific by TARGET_FAMILY"
    echo "# -------------------------------------------------------------"
    echo ""
    echo " ./build_all.sh                        # compile all available targets"
    echo " ./build.sh  --usage                   # display this menu"
    echo " ./build.sh  TARGET_FAMILY=8952 --prod # create customer build for 9x45"
    echo " ./build.sh  -c TARGET_FAMILY=8952     # clean 8952 binaries"
    echo " ./build.sh  TARGET_FAMILY=8952      # compile 8952  binaries"
    echo " ./build.sh                            # Error!! target name is missing"
    echo ""
    echo " # To build individual module , use the target specific build comand\n"
    echo " ./build_8852.sh sbl1                       # compiling 8952 sbl1\n"
    echo " ./build_8952.sh -c sbl1                    # cleaning 8952 sbl1\n"

    echo "==============================================================="
    sleep 2   
}
#-------- set the build dir and  reset the command line parameter ----- 
build_dir=`dirname $0`
cd $build_dir
#===============================================================================
#Set up  Environment 
#===============================================================================
usage


# if setenv.sh exist, run it.
[ -f $BUILD_ROOT/build/ms/setenv.sh ] && source setenv.sh || echo "Warning: SetEnv file not existed" 
#----- Parse target enviroment----
unset TARGET_FAMILY 
unset  BUILD_ID
unset cmds
isProd=0 

while [ $# -gt 0 ]
do
    case "$1" in
	BUILD_ID=*) 
		BUILD_ID=${1#BUILD_ID=}
		;;
	TARGET_FAMILY=*) 
		TARGET_FAMILY=${1#TARGET_FAMILY=}
		;;
	--usage) 
		usage
		exit 0
		;;	
	--prod) 
		isProd=1	
		cmds=${cmds//--prod/}
		if [ -e setenv.sh ] ; then #this is integrator build
			cmds="$cmds USES_FLAGS=USES_NO_STRIP_NO_ODM,USES_NO_DEBUG" 
		fi 
		;;
	*)
		cmds="$cmds $1"
    esac
    shift
done

source $TOOLS_SCONS_ROOT/build/rename-log.sh buildlog



#Enable warning=error 
cmds="CFLAGS=--diag_error=warning $cmds"


#8952 compilation on emmc
if [ "$TARGET_FAMILY" = "8952" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
	  #sh build_8952.sh boot jsdcc BUILD_ID=SAAAANAZ $cmds  
	  sh build_8952.sh boot jsdcc BUILD_ID=SAASANAZ $cmds
	  sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_LITE_SEC
	  sh build_8952.sh ddr_debug BUILD_ID=SAADANAZ $cmds
	else 
	  #sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=SAAAANAA $cmds
	  #sh build_8952.sh boot jsdcc BUILD_ID=SAAAANAA $cmds
	  sh build_8952.sh boot jsdcc BUILD_ID=SAASANAA $cmds
	  sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
	  sh build_8952.sh ddr_debug BUILD_ID=SAADANAA $cmds
	fi
   else
   if [[ $BUILD_ID =~ SAAAANAZ ]] 
	then
		sh build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAAAANAA ]] 
	then
		#sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAASANAZ ]] 
	then
		#sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAASANAA ]] 
	then
		#sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAADANAZ ]]
	then
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
		sh build_8952.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
		elif [[ $BUILD_ID =~ SAADANAA ]]
	then
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8952.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	fi
fi
fi

#8953 compilation on emmc
if [ "$TARGET_FAMILY" = "8953" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
		#sh build_8953.sh boot jsdcc BUILD_ID=JAAAANAZ $cmds  
		sh build_8953.sh boot jsdcc BUILD_ID=JAASANAZ $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_DDR
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_DDR_SEC
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_LITE
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_LITE_SEC
		sh build_8953.sh ddr_debug BUILD_ID=JAASANAZ $cmds
	else 
		#sh build_8953.sh boot jsdcc BUILD_ID=JAAAANAA $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=JAASANAA $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=JAAIANAA $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      	sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      	sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8953.sh ddr_debug BUILD_ID=JAASANAA $cmds
		sh build_8953.sh ddr_debug BUILD_ID=JAAIANAA $cmds
	fi
   else
   if [[ $BUILD_ID =~ JAAAANAZ ]] 
	then
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAAAANAA ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAASANAZ ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAASANAA ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAAIANAZ ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAAIANAA ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAADANAZ ]]
	then
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
	    sh build_8953.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAADANAA ]]
	then
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8953.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	fi
fi
fi

if [ "$TARGET_FAMILY" = "8976" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
	  sh build_8976.sh boot jsdcc BUILD_ID=EAAAANAZ $cmds
	  sh build_8976.sh boot jsdcc BUILD_ID=EAASANAZ $cmds
	  sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_LITE_SEC
	  sh build_8976.sh ddr_debug BUILD_ID=EAADANAZ $cmds
	else 
      sh build_8976.sh boot jsdcc BUILD_ID=EAAAANAA $cmds
      sh build_8976.sh boot jsdcc BUILD_ID=EAASANAA $cmds
	  sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
	  sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
	  sh build_8976.sh ddr_debug BUILD_ID=EAADANAA $cmds
	fi
   else
   
   if [[ $BUILD_ID =~ EAAAANAZ ]] 
	then
		sh build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAAAANAA ]] 
	then
		sh build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAASANAZ ]] 
	then
		build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAASANAA ]] 
	then
		sh build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAADANAZ ]]
	then
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
		sh build_8976.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAADANAA ]]
	then
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8976.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	fi
fi
fi

#8952 compilation on emmc
if [ "$TARGET_FAMILY" = "8952_8976" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
	  #sh build_8952.sh boot jsdcc BUILD_ID=SAAAANAZ $cmds  
	  sh build_8952.sh boot jsdcc BUILD_ID=SAASANAZ $cmds
	  sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAZ $cmds,USES_DEVPRO_LITE_SEC
	  sh build_8952.sh ddr_debug BUILD_ID=SAADANAZ $cmds
	  #sh build_8976.sh boot jsdcc BUILD_ID=EAAAANAZ $cmds
	  sh build_8976.sh boot jsdcc BUILD_ID=EAASANAZ $cmds
	  sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAZ $cmds,USES_DEVPRO_LITE_SEC
	  sh build_8976.sh ddr_debug BUILD_ID=EAADANAZ $cmds
	else 
	  #sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=SAAAANAA $cmds
	  sh build_8952.sh boot jsdcc BUILD_ID=SAAAANAA $cmds
	  sh build_8952.sh boot jsdcc BUILD_ID=SAASANAA $cmds
	  sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8952.sh deviceprogrammer_ddr BUILD_ID=SAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
	  sh build_8952.sh ddr_debug BUILD_ID=SAADANAA $cmds
	  
	  #sh build_8976.sh boot jsdcc BUILD_ID=EAAAANAA $cmds
      sh build_8976.sh boot jsdcc BUILD_ID=EAASANAA $cmds
	  sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
	  sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8976.sh deviceprogrammer_ddr BUILD_ID=EAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
	  sh build_8976.sh ddr_debug BUILD_ID=EAADANAA $cmds
	fi
   else
   if [[ $BUILD_ID =~ EAAAANAZ ]] 
	then
		sh build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAAAANAA ]] 
	then
		sh build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAASANAZ ]] 
	then
		build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAASANAA ]] 
	then
		sh build_8976.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAADANAZ ]]
	then
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
		sh build_8976.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ EAADANAA ]]
	then
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8976.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8976.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAAAANAZ ]] 
	then
		sh build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAAAANAA ]] 
	then
		#sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAASANAZ ]] 
	then
		#sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAASANAA ]] 
	then
		#sh build_8952.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		build_8952.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAADANAZ ]]
	then
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
		sh build_8952.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ SAADANAA ]]
	then
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8952.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8952.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
   fi
fi
fi
if [ "$TARGET_FAMILY" = "8937_8917" ]
then
    if [ -z $BUILD_ID ] 
    then
	 if [ $isProd -gt 0 ]
	then 
	  #sh build_8937.sh boot jsdcc BUILD_ID=FAADANAZ $cmds  
	  sh build_8937.sh boot jsdcc BUILD_ID=FAASANAZ $cmds
	  sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE_SEC
      sh build_8937.sh ddr_debug BUILD_ID=FAADANAZ $cmds

	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAZ $cmds
	  sh build_8917.sh boot jsdcc BUILD_ID=LAASANAZ $cmds
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAZ $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_LITE_SEC
      sh build_8917.sh ddr_debug BUILD_ID=LAADANAZ $cmds
	else 
	  #sh build_8937.sh boot jsdcc deviceprogrammer BUILD_ID=FAASANAA $cmds
	  sh build_8937.sh boot jsdcc BUILD_ID=FAASANAA $cmds
	  sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
      sh build_8937.sh ddr_debug BUILD_ID=FAADANAA $cmds
	  
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAA $cmds
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAA $cmds
      sh build_8917.sh boot jsdcc BUILD_ID=LAASANAA $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
      sh build_8917.sh ddr_debug BUILD_ID=LAADANAA $cmds
	fi
  else
   if [[ $BUILD_ID =~ LAAAANAZ ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAAAANAA ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAASANAZ ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAASANAA ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAADANAZ ]]
	then
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
        sh build_8917.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAADANAA ]]
	then
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8917.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAADANAZ ]] 
	then
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR_SEC
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE_SEC
        sh build_8937.sh ddr_debug BUILD_ID=FAADANAZ $cmds
	elif [[ $BUILD_ID =~ FAASANAA ]] 
	then
		#sh build_8937.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAASANAZ ]] 
	then
		#sh build_8937.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAADANAA ]]
	then
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8937.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
    fi
fi
fi

if [ "$TARGET_FAMILY" = "8937_8917_8953" ]
then
    if [ -z $BUILD_ID ] 
    then
	 if [ $isProd -gt 0 ]
	then 
	  #sh build_8937.sh boot jsdcc BUILD_ID=FAADANAZ $cmds  
	  sh build_8937.sh boot jsdcc BUILD_ID=FAASANAZ $cmds
	  sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE_SEC
      sh build_8937.sh ddr_debug BUILD_ID=FAADANAZ $cmds

	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAZ $cmds
	  sh build_8917.sh boot jsdcc BUILD_ID=LAASANAZ $cmds
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAZ $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_LITE_SEC
      sh build_8917.sh ddr_debug BUILD_ID=LAADANAZ $cmds
	  
	  sh build_8953.sh boot jsdcc BUILD_ID=JAASANAZ $cmds
	  sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_DDR
	  sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_DDR_SEC
	  sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_LITE
	  sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAZ $cmds,USES_DEVPRO_LITE_SEC
	  sh build_8953.sh ddr_debug BUILD_ID=JAASANAZ $cmds
	  
	else 
	  #sh build_8937.sh boot jsdcc deviceprogrammer BUILD_ID=FAASANAA $cmds
	  sh build_8937.sh boot jsdcc BUILD_ID=FAASANAA $cmds
	  sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
      sh build_8937.sh ddr_debug BUILD_ID=FAADANAA $cmds
	  
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAA $cmds
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAA $cmds
      sh build_8917.sh boot jsdcc BUILD_ID=LAASANAA $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
      sh build_8917.sh ddr_debug BUILD_ID=LAADANAA $cmds
	  
	  sh build_8953.sh boot jsdcc BUILD_ID=JAASANAA $cmds
	  sh build_8953.sh boot jsdcc BUILD_ID=JAAIANAA $cmds
	  sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
	  sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8953.sh deviceprogrammer_ddr BUILD_ID=JAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
	  sh build_8953.sh ddr_debug BUILD_ID=JAASANAA $cmds
	  sh build_8953.sh ddr_debug BUILD_ID=JAAIANAA $cmds
	fi
  else
   if [[ $BUILD_ID =~ LAAAANAZ ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAAAANAA ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAASANAZ ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAASANAA ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAADANAZ ]]
	then
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
        sh build_8917.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAADANAA ]]
	then
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8917.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAADANAZ ]] 
	then
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR_SEC
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE_SEC
        sh build_8937.sh ddr_debug BUILD_ID=FAADANAZ $cmds
	elif [[ $BUILD_ID =~ FAASANAA ]] 
	then
		#sh build_8937.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAASANAZ ]] 
	then
		#sh build_8937.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAADANAA ]]
	then
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8937.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAAAANAZ ]] 
	then
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAAAANAA ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAASANAZ ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAASANAA ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
    	elif [[ $BUILD_ID =~ JAAIANAZ ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAAIANAA ]] 
	then
		#sh build_8953.sh boot jsdcc deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8953.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAADANAZ ]]
	then
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
	    sh build_8953.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ JAADANAA ]]
	then
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8953.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8953.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
    fi
fi
fi

if [ "$TARGET_FAMILY" = "8937" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
	  #sh build_8937.sh boot jsdcc BUILD_ID=FAAAANAZ $cmds  
	  sh build_8937.sh boot jsdcc BUILD_ID=FAASANAZ $cmds
	  sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAZ $cmds,USES_DEVPRO_LITE_SEC
	  sh build_8937.sh ddr_debug BUILD_ID=FAADANAZ $cmds
	else 
	  #sh build_8937.sh boot jsdcc BUILD_ID=FAAAANAA $cmds
	  sh build_8937.sh boot jsdcc BUILD_ID=FAASANAA $cmds
	  sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8937.sh deviceprogrammer_ddr BUILD_ID=FAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
	  sh build_8937.sh ddr_debug BUILD_ID=FAADANAA $cmds
	fi
   else
   if [[ $BUILD_ID =~ FAAAANAZ ]] 
	then
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAAAANAA ]] 
	then
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAASANAZ ]] 
	then
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAASANAA ]] 
	then
		sh build_8937.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAADANAZ ]]
	then
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
		sh build_8937.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ FAADANAA ]]
	then
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8937.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8937.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	fi
fi
fi

if [ "$TARGET_FAMILY" = "8917" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAZ $cmds  
	  sh build_8917.sh boot jsdcc BUILD_ID=LAASANAZ $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_DDR
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_DDR_SEC
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_LITE
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAZ $cmds,USES_DEVPRO_LITE_SEC
      sh build_8917.sh ddr_debug BUILD_ID=LAADANAZ $cmds
	else 
	  #sh build_8917.sh boot jsdcc BUILD_ID=LAAAANAA $cmds
	  sh build_8917.sh boot jsdcc BUILD_ID=LAASANAA $cmds
	  sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE $cmds
      sh build_8917.sh deviceprogrammer_ddr BUILD_ID=LAADANAA USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
      sh build_8917.sh ddr_debug BUILD_ID=LAADANAA $cmds
	fi
   else
   if [[ $BUILD_ID =~ LAAAANAZ ]] 
	then
		sh build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAAAANAA ]] 
	then
		build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAASANAZ ]] 
	then
		build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAASANAA ]] 
	then
		build_8917.sh boot jsdcc BUILD_ID=$BUILD_ID $cmds
	elif [[ $BUILD_ID =~ LAADANAZ ]]
	then
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_DDR_SEC
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE
        sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID $cmds,USES_DEVPRO_LITE_SEC
        sh build_8917.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
		elif [[ $BUILD_ID =~ LAADANAA ]]
	then
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_DDR_SEC $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE $cmds
		sh build_8917.sh deviceprogrammer_ddr BUILD_ID=$BUILD_ID USES_FLAGS=USES_DEVPRO_LITE_SEC $cmds
		sh build_8917.sh ddr_debug BUILD_ID=$BUILD_ID $cmds
	fi
fi
fi
