
PRODUCT_PACKAGES += \
    DeviceInfo \
    init.carrier.rc \
    qcril.db \
    libapkscanner \
    PhoneFeatures \
    com.qrd.wappush \
    com.qrd.wappush.xml \
    wrapper-updater \
    CarrierConfigure \
    CarrierLoadService \
    CarrierCacheService \
    NotificationService \
    TouchPal_Global \
    ArabicPack \
    BengaliPack \
    CangjiePack \
    ChtPack \
    HindiPack \
    IndonesianPack \
    MarathiPack \
    PortuguesebrPack \
    RussianPack \
    SpanishLatinPack \
    TagalogPack \
    TamilPack \
    TeluguPack \
    ThaiPack \
    VietnamPack \
    ProfileMgr \
    ExtSettings \
    SnapdragonSVA \
    TimerSwitch \
    QSService \
    PowerOffHandler \
    PowerOffHandlerApp \
    QTITaskManager \
    com.qualcomm.qti.smartsearch.xml \
    Firewall \
    LauncherUnreadService \
    NetworkControl \
    PowerOnAlert \
    StorageCleaner \
    DataStorageCleanerService \
    ProfileMgr \
    LunarInfoProvider \
    ZeroBalanceHelper \
    libdatactrl \
    Setup_Wizard
ifneq ($(call is-board-platform-in-list, msm8998, msm8998_32),true)
  PRODUCT_PACKAGES += \
        ConfigurationClient \
        OmaDownload \
        libomadrmengine \
        libomadrmutils_jni \
        OmaDrmEngine \
        OmaDrmEngineServer \
        ConfigurationClient
endif

PRODUCT_PACKAGE_OVERLAYS += vendor/qcom/proprietary/qrdplus/Extension/apps/BatterySaver/overlay

