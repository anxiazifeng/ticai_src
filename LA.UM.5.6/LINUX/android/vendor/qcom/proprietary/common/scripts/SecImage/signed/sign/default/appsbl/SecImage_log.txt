l1_file_name = /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Encryption/Unified/default/l1_key.bin
l2_file_name = /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Encryption/Unified/default/l2_key.bin
l3_file_name = /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Encryption/Unified/default/l3_key.bin
Clear L1 key, clear L2 key, and clear L3 keys were provided locally.
Performing OEM sign on image: /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/out/target/product/msm8953_64/emmc_appsboot.mbn
attestation_certificate_extensions = /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/General_Assets/Signing/openssl/v3_attest.ext
ca_certificate_extensions = /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/General_Assets/Signing/openssl/v3.ext
openssl_configfile = /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/General_Assets/Signing/openssl/opensslroot.cfg
Using QC HMAC for hash segment
Using PKCS RSA padding
Using a predefined Root certificate and a predefined key
Key Used: /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Signing/Local/qc_presigned_certs-key2048_exp65537/qpsa_rootca.key
Certificate Used: /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Signing/Local/qc_presigned_certs-key2048_exp65537/qpsa_rootca.cer
Using a predefined Attestation CA certificate and a predefined key
Key Used: /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Signing/Local/qc_presigned_certs-key2048_exp65537/qpsa_attestca.key
Certificate Used: /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Signing/Local/qc_presigned_certs-key2048_exp65537/qpsa_attestca.cer
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID     | 0x0000000000000009  |
| HW_ID     | 0x0000000000000000  |
| DEBUG     | 0x0000000000000002  |
| OEM_ID    | 0x0000              |
| SW_SIZE   | 168                 |
| MODEL_ID  | 0x0000              |
| SHA_ALGO  | SHA256              |
| APP_ID    | None                |
| CRASH_DUMP| None                |
| ROT_EN    | None                |
| Exponent  | 65537               |
| TCG_MIN   | None                |
| TCG_MAX   | None                |
| FID_MIN   | None                |
| FID_MAX   | None                |

Signed image is stored at /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/signed/sign/default/appsbl/emmc_appsboot.mbn
Clear L1 key, clear L2 key, and clear L3 keys were provided locally.
image is signed with PKCS
Image /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/signed/sign/default/appsbl/emmc_appsboot.mbn signature is valid
Image /home/szzt/sc60/SC60_Android_7.1.2_prebuilt_for_QFIL_20170829/LA.UM.5.6/LINUX/android/vendor/qcom/proprietary/common/scripts/SecImage/signed/sign/default/appsbl/emmc_appsboot.mbn is not encrypted

Base Properties: 
| Integrity Check                 | True  |
| Signed                          | True  |
| Encrypted                       | False |
| Size of signature               | 256   |
| Size of one cert                | 2048  |
| Num of certs in cert chain      | 3     |
| Number of root certs            | 1     |
| Hash Page Segments as segments  | False |
| Cert chain size                 | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x8f600000                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 2                              |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No |    Type    | Offset | VirtAddr | PhysAddr | FileSize | MemSize | Flags | Align |
|------|------------|--------|----------|----------|----------|---------|-------|-------|
|  1   | 1879048193 |0x84964 |0x8f67c964|0x8f67c964| 0x00020  | 0x00020 |  0x4  | 0x4   |
|  2   |    LOAD    |0x08000 |0x8f600000|0x8f600000| 0x8d618  | 0xa0b38 |  0x7  | 0x8000|

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0x8f67d1a8  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000080  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x8f67d028  |
| image_id        | 0x00000000  |
| image_size      | 0x00001980  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x8f67d0a8  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type        | 0     |
| max_elf_segments  | 100   |
| testsig_serialnum | None  |

