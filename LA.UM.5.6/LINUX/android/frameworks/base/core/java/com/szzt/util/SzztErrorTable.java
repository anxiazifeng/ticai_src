package com.szzt.util;

import android.text.TextUtils;

/**
 * Created by yxljl1314 on 2017/12/13.
 */

public class SzztErrorTable {
    public enum ErrorTable{
        Success(0,"Success",true),
        ParameterError(2001,"Parameter Error",false),
        NoAuthority(2002,"No Authority",false),
        FileCreateFail(2003,"File Create Fail",false),
        FileNoexists(2004,"File no exists",false),
        ConnectFail(2005,"Connect Fail",false),
        NullObject(2006,"Null Object",false),
        OperationFail(2007,"Operation Fail",false),
        StatusError(2008,"Status Error",false),
        FileNoCanRead(2009,"File no can read",false),
        SignatureError(2010,"Signature Error",false),
        VersionMismatch(2011,"Version number mismatch",false),
        BindFail(2012,"Bindings failure",false),
        UnknownAnomaly(9999,"Unknown Anomaly",false);

        private int code;
        private String name;
        private boolean resultBoolean;
        private String describeMsg;
        private static ErrorTable lastOperationError = UnknownAnomaly;
        private ErrorTable(int index,String name,boolean booleanResult) {
            this.code = index;
            this.name = name;
            this.describeMsg = name;
            this.resultBoolean = booleanResult;
        }
        @Override
        public String toString() {
            lastOperationError = this;
            if(this.code != 0){
                if(TextUtils.isEmpty(describeMsg)){
                    this.describeMsg = this.name;
                }
            }else{
                this.describeMsg = "";
            }
            return this.describeMsg;
        }
        public String toString(String extraMsg) {
            lastOperationError = this;
            if(this.code != 0){
                this.describeMsg = this.name+":"+extraMsg;
            }else{
                this.describeMsg = "";
            }
            return this.describeMsg;
        }
        public int getErrorCode(){
            lastOperationError = this;
            return this.code;
        }
        public boolean getErrorBooleanCode(){
            lastOperationError = this;
            return this.resultBoolean;
        }

        public static int getLastOperationErrorCode(){
            return lastOperationError.getErrorCode();
        }

        public static String getLastOperationErrorString(){
            return lastOperationError.toString();
        }
    }
}
