//============================================================================
//  Name:                                                                     
//    std_loadbuild_wp.cmm 
//
//  Description:                                                              
//    WA Specific Build loading script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 04/06/2015 JBILLING      changed path for devcfgtz back now that workaround finished.
// 04/01/2015 JBILLING      changed path for devcfgtz
// 03/17/2015 JBILLING      Added TZDevcfg
// 01/26/2015 JBILLING      Changed for 8996. Removed SDI
// 07/01/2014 AJCheriyan    Added change to load pmic.mbn
// 02/21/2013 AJCheriyan    Changed SDI binary name
// 01/18/2013 AJCheriyan    Added TZAPPS binary
// 08/13/2012 AJCheriyan    Ported from 8974 LA, Added FAT16 binary
// 08/08/2012 AJCheriyan    Fixed issue with paths for mjsdload
// 07/19/2012 AJCheriyan    Created for B-family 

// ARG0 - Load option - Supported : ERASEONLY, LOADCOMMON, LOADFULL, LOADIMG
// ARG1 - Valid image name. Can be used only with LOADIMG option.
ENTRY &ARG0 &ARG1

LOCAL &CWD &SEARCHPATHS &BINARY &BINARYPATH &PROGRAMMER &STORAGE_TYPE

MAIN:
    // We have checked for all the intercom sessions at this point and we don't need any error
    // Save the argument
    &LOAD_OPTION="&ARG0"


    //For WP, devcfg binary lives in apps. may change later
    LOCAL &DEVCFG_BINARY_BUILDROOT
    //&DEVCFG_BINARY_BUILDROOT="&APPS_BUILDROOT"
    &DEVCFG_BINARY_BUILDROOT="&TZ_BUILDROOT"


    // Switch to the tools directory
    &CWD=OS.PWD()


    // Check for the boot option
    do hwio 
    
    
    do std_utils HWIO_INF BOOT_CONFIG FAST_BOOT
    ENTRY &STORAGE_OPTION
    
    
    IF (&STORAGE_OPTION==0x4) //0x4 UFS
    (
    
        IF (OS.DIR("&BOOT_BUILDROOT/QcomPkg"))
        (
            &PROGRAMMER="QcomPkg/Tools/storage/UFS/jtagprogrammer"
        )
        ELSE
        (
            &PROGRAMMER="boot_images/QcomPkg/Tools/storage/UFS/jtagprogrammer"
        )
        &XML_LOCATION="&METASCRIPTSDIR/../../../build/ufs"
        &STORAGE_TYPE="ufs"
        &MAX_PARTITIONS=6
        
        PRINT %ERROR "Warning - WP scripts not yet configured for UFS"

    )
    ELSE //else EMMC 
    (

        IF (OS.DIR("&BOOT_BUILDROOT/QcomPkg"))
        (
            &PROGRAMMER="QcomPkg/Tools/storage/eMMC/jtagprogrammer"
        )
        ELSE
        (
            &PROGRAMMER="boot_images/QcomPkg/Tools/storage/eMMC/jtagprogrammer"
        )
        
        &XML_LOCATION="&METASCRIPTSDIR/../../../build/emmc"
        &STORAGE_TYPE="emmc"
        &MAX_PARTITIONS=1

    )


    // Erase only
    IF (("&ARG0"=="ERASEONLY")||("&ARG0"=="LOADCOMMON")||("&ARG0"=="LOADFULL"))
    (
        // Only erase the chip and exit
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER ERASE
    )

    // Load common images
    IF (("&ARG0"=="LOADCOMMON")||("&ARG0"=="LOADFULL"))
    (
        // Check for all the common images 

        // Check for the presence of all the binaries
        // Not needed because meta-build should have populated all this information
        // SBL + PMIC, TZ, RPM, APPSBL\
        //boot: xbl.elf, pmic.elf
        do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&BOOT_ELF
        do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&PMIC_ELF
        //rpm: rpm.mbn
        do std_utils FILEXIST FATALEXIT &RPM_BUILDROOT/&RPM_BINARY
        //tz: tz.mbn,hyp.mbn
        do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&TZ_BINARY
        do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&HYP_BINARY
        //winsecapp.mbn, uefi_sec.mbn
        do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&WINSECAPP_BINARY
        do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&UEFISEC_BINARY
        do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&APPSCONFIG_BINARY
        do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&EFIESP_BINARY
        do std_utils FILEXIST FATALEXIT &DEVCFG_BINARY_BUILDROOT/&TZDEVCFG_BINARY
        

        
        
        // Now flash them all one by one 
        // Flash the partition table
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE"
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=gpt_main0.bin,gpt_backup0.bin
    
        // Apply the disk patches
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE"
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER PATCH searchpaths=&SEARCHPATHS xml=patch0.xml
        
        
        
        // XBL + PMIC
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+OS.FILE.PATH(&BOOT_BUILDROOT/&BOOT_ELF)+","+OS.FILE.PATH(&BOOT_BUILDROOT/&PMIC_ELF)
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=xbl.elf,pmic.elf
        
        // RPM
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+OS.FILE.PATH(&RPM_BUILDROOT/&RPM_BINARY)
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=rpm.mbn
        
        // TZ,HYP
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+OS.FILE.PATH(&TZ_BUILDROOT/&TZ_BINARY)+","+OS.FILE.PATH(&TZ_BUILDROOT/&HYP_BINARY)+","+OS.FILE.PATH(&DEVCFG_BINARY_BUILDROOT/&TZDEVCFG_BINARY)
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=tz.mbn,hyp.mbn,devcfg.mbn
        
        //winsecapp.mbn, uefi_sec.mbn
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+OS.FILE.PATH(&APPS_BUILDROOT/&WINSECAPP_BINARY)+","+OS.FILE.PATH(&APPS_BUILDROOT/&UEFISEC_BINARY)
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=winsecapp.mbn,tzapps.bin
        
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+OS.FILE.PATH(&APPS_BUILDROOT/&EFIESP_BINARY)
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=EFIESP.bin

        // Apps Configuration Info / ACPI binary
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+OS.FILE.PATH(&APPS_BUILDROOT/&APPSCONFIG_BINARY)
        CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=fat16.bin

    )
    
    // Load common images
    IF ("&ARG0"=="LOADIMG")
    (
        // Check for the binary first 
        IF ("&ARG1"=="xbl")
        (
            do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&BOOT_ELF
            do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&PMIC_ELF
            &BINARY="xbl.elf,pmic.elf"
            &BINARYPATH=OS.FILE.PATH(&BOOT_BUILDROOT/&BOOT_ELF)+","+OS.FILE.PATH(&BOOT_BUILDROOT/&PMIC_ELF)
        )
        IF ("&ARG1"=="tz")
        (
            do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&TZ_BINARY
            do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&HYP_BINARY
            do std_utils FILEXIST FATALEXIT &DEVCFG_BINARY_BUILDROOT/&TZDEVCFG_BINARY
            
            &BINARY="tz.mbn,hyp.mbn,devcfg.mbn"
            &BINARYPATH=OS.FILE.PATH("&TZ_BUILDROOT/&TZ_BINARY")+","+OS.FILE.PATH("&TZ_BUILDROOT/&HYP_BINARY")+","+OS.FILE.PATH("&DEVCFG_BINARY_BUILDROOT/&TZDEVCFG_BINARY")
        )
        IF ("&ARG1"=="rpm")
        (
            do std_utils FILEXIST FATALEXIT &RPM_BUILDROOT/&RPM_BINARY
            &BINARY="rpm.mbn"
            &BINARYPATH=OS.FILE.PATH("&RPM_BUILDROOT/&RPM_BINARY")
        )
        
        IF ("&ARG1"=="appsboot")
        (
            do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&WINSECAPP_BINARY
            do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&UEFISEC_BINARY

            
            &BINARY="winsecapp.mbn,tzapps.bin"
            &BINARYPATH=OS.FILE.PATH("&APPS_BUILDROOT/&WINSECAPP_BINARY")+","+OS.FILE.PATH("&APPS_BUILDROOT/&UEFISEC_BINARY")
        )
        
        // Flash the image now
        &SEARCHPATHS="&METASCRIPTSDIR/../../../build/&STORAGE_TYPE,"+"&BINARYPATH"
        &PARTITION=0
        WHILE (&PARTITION<1)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=rawprogram0.xml files=&BINARY
            &PARTITION=&PARTITION+1
        )
    )

    // Load HLOS images
    IF ("&LOAD_OPTION"=="LOADFULL")
    (
        // Call the HLOS specific loading script
        do std_utils FILEXIST FATALEXIT &METASCRIPTSDIR/../../../build/loadbuild_wp.py
        OS.COMMAND cmd /k python &METASCRIPTSDIR/../../../build/loadbuild_wp.py 
             
    )

    // Return to the old directory
    CD &CWD

    GOTO EXIT


FATALEXIT:
    END

EXIT:
    ENDDO



    
    
    

        

